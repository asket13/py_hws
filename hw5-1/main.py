def count_items_in_file():
    item_counts = {}

    with open("orders", 'r', encoding='utf-8') as file:
        content = file.read()

    content = content.replace('\n', ' ').replace('\r', ' ')

    items = content.split('@@@')

    for item in items:
        item = item.strip()
        if item:
            if item in item_counts:
                item_counts[item] += 1
            else:
                item_counts[item] = 1

    sorted_item_counts = dict(sorted(item_counts.items(), key=lambda x: x[1], reverse=True))

    return sorted_item_counts

item_counts = count_items_in_file()

for item, count in item_counts.items():
    print(f"{item}: {count}")
