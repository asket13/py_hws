# Завдання 1
print("Завдання 1")

while True:
    try:
        number = int(input("Введіть ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

if number % 3 == 0 and number % 5 == 0:
    print("ham")
elif number % 3 == 0:
    print("foo")
elif number % 5 == 0:
    print("bar")
else:
    print("Число не ділиться ні на 3, ні на 5.")

##############################################################################################
# Завдання 2
print("\nЗавдання 2")

while True:
    try:
        num1 = int(input("Введіть перше ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

while True:
    try:
        num2 = int(input("Введіть друге ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

if num1 < num2:
    print(f"Менше число: {num1}")
    print(f"Більше число: {num2}")
elif num1 > num2:
    print(f"Менше число: {num2}")
    print(f"Більше число: {num1}")
else:
    print("Числа рівні.")

##############################################################################################
# Завдання 3
print("\nЗавдання 3")

while True:
    try:
        num1 = int(input("Введіть перше ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

while True:
    try:
        num2 = int(input("Введіть друге ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

while True:
    try:
        num3 = int(input("Введіть третє ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

numbers = [num1, num2, num3]
numbers.sort()

print(f"Найменше число: {numbers[0]}")
print(f"Середнє число: {numbers[1]}")
print(f"Найбільше число: {numbers[2]}")

##############################################################################################
# Завдання 4
print("\nЗавдання 4")

while True:
    command = input("Введіть ціле число для гри Fizz(%3)-Buzz(%5) або 'вийти' для завершення програми: ").lower()

    if command == "вийти":
        print("Програма завершена.")
        break

    try:
        number = int(command)
        if number % 3 == 0 and number % 5 == 0:
            print("fizz buzz")
        elif number % 3 == 0:
            print("fizz")
        elif number % 5 == 0:
            print("buzz")
        else:
            print(f"знімай картуз: {number}")
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число або 'вийти' для завершення програми.")
##############################################################################################
# Завдання 5
print("\nЗавдання 5")

while True:
    command = input("Введіть ціле число для гри 7-boom або 'вийти' для завершення програми: ").lower()

    if command == "вийти":
        print("Програма завершена.")
        break

    try:
        number = int(command)
        if number % 7 == 0 or '7' in str(number):
            print("BOOM BOOM BOOM")
        else:
            print(f":(P: {number}")
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число або 'вийти' для завершення програми.")