def simple_prime_check(n):
    """
    Calculate the prime values for a given range using the straightforward method.
    :param n: upper limit of the range (inclusive)
    :return: list of prime values
    """
    primes = []
    for num in range(2, n + 1):
        is_prime = True
        for i in range(2, num):
            if num % i == 0:
                is_prime = False
                break
        if is_prime:
            primes.append(num)
    print(primes)
    return primes

def sieve_of_eratosthenes(n):
    """
    Calculate the prime values for a given range using the Sieve of Eratosthenes method.
    :param n: upper limit of the range (inclusive)
    :return: list of prime values
    """
    is_prime = [True] * ( n + 1 )
    p = 2
    while (p * p <= n):
        if is_prime[p] == True:
            for i in range(p * p, n + 1, p):
                is_prime[i] = False
        p += 1
    primes = [p for p in range(2, n + 1) if is_prime[p]]
    print(primes)
    return primes