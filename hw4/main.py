import time
from primes import simple_prime_check, sieve_of_eratosthenes
def time_function(func, r):
    start_time = time.time()
    result = func(r)
    end_time = time.time()
    return end_time - start_time, result


ranges = [100, 1000, 10000]

for r in ranges:
    print(f"\nПеревірка діапазону до {r}")

    simple_time, simple_primes = time_function(simple_prime_check, r)
    print(f"Простий метод: {simple_time:.4f} секунд")

    sieve_time, sieve_primes = time_function(sieve_of_eratosthenes, r)
    print(f"Решето Ератосфена: {sieve_time:.4f} секунд")
