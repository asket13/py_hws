exam1 = [
    ("student1", 78, "failed"),
    ("student2", 82, "passed"),
    ("student3", 97, "passed"),
    ("student4", 86, "passed"),
    ("student5", 99, "passed"),
    ("student6", 75, "passed")
]
exam2 = [
    ("student1", 84, "passed"),
    ("student2", 78, "passed"),
    ("student3", 65, "failed"),
    ("student4", 90, "passed"),
    ("student5", 72, "failed")
]
exam3 = [
    ("student1", 82, "passed"),
    ("student2", 75, "passed"),
    ("student3", 65, "failed"),
    ("student4", 67, "failed"),
    ("student5", 69, "failed"),
    ("student6", 79, "failed"),
    ("student7", 78, "passed"),
]
exam4 = [
    ("student1", 82, "passed"),
]
exam5 = [
]
exam6 = [
    ("student1", 82, "passed"),
    ("student2", 75, "passed"),
    ("student3", 65, "passed"),
]
exam7 = [
    ("student1", 82, "failed"),
    ("student2", 75, "failed"),
    ("student3", 65, "failed"),
]
