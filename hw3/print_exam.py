from tabulate import tabulate
def print_exam(data, exam_number):
    headers = ["Name", "Score", "Status"]
    print(f"\n---------------------------------Іспит №{exam_number}:---------------------------------")
    print(tabulate(data, headers=headers, tablefmt="grid"))