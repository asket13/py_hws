from hw3.checker_func import check_consistency
from hw3.print_exam import print_exam
from hw3.data import exam1, exam2, exam3, exam4, exam5, exam6, exam7

exams = [exam1, exam2, exam3, exam4, exam5, exam6, exam7]

for i, exam in enumerate(exams, 1):
    print_exam(exam, i)
    check_consistency(exam)