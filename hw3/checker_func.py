def check_consistency(data):
    passed_students = [(name, score) for name, score, status in data if status == "passed"]
    failed_students = [(name, score) for name, score, status in data if status == "failed"]

    inconsistent_pairs = []

    for name_f, score_f in failed_students:
        for name_p, score_p in passed_students:
            if score_f >= score_p:
                inconsistent_pairs.append((name_f, score_f, name_p, score_p))

    if inconsistent_pairs:
        print("Професор Грубл був непослідовним.")
        for name_f, score_f, name_p, score_p in inconsistent_pairs:
            print(f"Непослідовність: {name_f} з оцінкою {score_f} (failed) та {name_p} з оцінкою {score_p} (passed)")
    else:
        if passed_students:
            min_passed_score = min(score for name, score in passed_students)
        else:
            min_passed_score = None

        if failed_students:
            max_failed_score = max(score for name, score in failed_students)
        else:
            max_failed_score = None

        if min_passed_score is not None and max_failed_score is not None:
            print("Професор Грубл був послідовним.")
            print(f"Поріг для складання іспиту знаходиться в діапазоні {max_failed_score + 1} – {min_passed_score} балів.")
        elif min_passed_score is None and max_failed_score is None:
            print("На іспит не прийшло жодного студенту або сам професор Грубл десь загуляв")
        else:
            if min_passed_score:
                print(f"усі здали і мінімальний бал складання {min_passed_score}")
            else:
                print(F"усі не здали і максимальний бал нездачі {max_failed_score}")
