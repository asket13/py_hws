# Завдання 1
name = input("Завдання 1\nВведіть своє ім'я: ")
while True:
    try:
        monthly_salary = float(input("Введіть свою місячну зарплату в доларах: "))
        if monthly_salary < 0:
            print("Зарплата не може бути від'ємною. Будь ласка, введіть коректне значення.")
        else:
            break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть числове значення.")

annual_salary = monthly_salary * 12 / 1000
print(f"Річна зарплата {name} складає {annual_salary:.0f} тис. доларів\n")

##################################################################################################
# Завдання 2
print("Завдання 2")
while True:
    try:
        number = int(input("Введіть ціле число від 100 до 999: "))
        if 100 <= number <= 999:
            break
        else:
            print(f"{number} в інтервалі від 100 до 999: {100 <= number <= 999}")
            print("Число повинно бути в діапазоні від 100 до 999.")
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число від 100 до 999.")
print(f"{number} в інтервалі від 100 до 999: {100 <= number <= 999}\n")

##################################################################################################
# Завдання 3
reversed_num = int(str(number)[::-1])
print(f"Завдання 3\nреверсоване число: {reversed_num}\n")

##################################################################################################
# Завдання 4
print("Завдання 4")
while True:
    try:
        num1 = int(input("Введіть перше ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

while True:
    try:
        num2 = int(input("Введіть друге ціле число: "))
        break
    except ValueError:
        print("Некоректне значення. Будь ласка, введіть ціле число.")

print(f"Перше число: {num1}")
print(f"Друге число: {num2}")

sum_result = num1 + num2
print(f"Сума: {sum_result}")

difference = num1 - num2
print(f"Різниця: {difference}")

product = num1 * num2
print(f"Результат множення: {product}")

if num2 != 0:
    division = num1 / num2
    print(f"Результат поділу першого на друге: {division:.2f}")
else:
    print("Ділення на нуль неможливе")

if num2 != 0:
    remainder = num1 % num2
    print(f"Залишок від поділу першого на друге: {remainder}")
else:
    print("Обчислення залишку від ділення на нуль неможливе")

comparison = num1 >= num2
print(f"{num1} >= {num2} : {comparison}")
